// import { config } from "../../config";
import React, { useCallback, useEffect, useState } from "react";
import { Button, Card, Container } from "react-bootstrap";
import DataTable from "react-data-table-component";
import { useSelector } from "react-redux";
import { useParams } from "react-router";
import { getInvoiceByOrderId } from "../../app/api/order";
import { formatRupiah } from "../../utils";
import axios from "axios";

export default function Invoices() {
  const { id } = useParams();
  const [invoice, setInvoice] = useState([]);
  const [isFetching, setIsFetching] = useState(false);
  const auth = useSelector((state) => state.auth);

  const [token, setToken] = useState("");

  const builderData = useCallback(
    (data) => {
      return [
        { label: "Status", value: data.payment_status },
        { label: "Order ID", value: data._id },
        { label: "Total", value: formatRupiah(data.total) },
        {
          label: "Dikirim",
          value: (
            <div>
              <br />
              <strong>{auth.user.full_name}</strong>
              <br />
              {auth.user.email}
              <br />
              <br />
              {data.delivery_address.provinsi},{" "}
              {data.delivery_address.kabupaten},{" "}
              {data.delivery_address.kecamatan},{" "}
              {data.delivery_address.kelurahan}
              <br />({data.delivery_address.detail})
              <br />
              <br />
            </div>
          ),
        },
        // {
        //   label: "Payment to",
        //   value: (
        //     <div>
        //       <br />
        //       {config.owner}
        //       <br />
        //       {config.contact}
        //       <br />
        //       {config.billing.bank_name}
        //       <br />
        //       {config.billing.account_no}
        //       <br />
        //     </div>
        //   ),
        // },
      ];
    },

    [auth.user.full_name, auth.user.email]
  );

  useEffect(() => {
    setIsFetching(true);
    getInvoiceByOrderId(id)
      .then(({ data }) => setInvoice(builderData(data)))
      .finally((_) => setIsFetching(false));
  }, [id, builderData]);

  const process = async () => {
    const dataPay = {
      name: auth.user.full_name,
      order_id: invoice[1].value,
      total: parseInt(invoice[2].value.replace(/[^\d]/g, ""), 10),
    };
    console.log(dataPay);

    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await axios.post(
      "http://localhost:3000/api/payment/process-transaction",
      dataPay,
      config
    );

    setToken(response.data.token);
  };

  useEffect(() => {
    if (token) {
      window.snap.pay(token, {
        onSuccess: function (result) {
          console.log("success");
          console.log(result);
        },
        onPending: function (result) {
          console.log("pending");
          console.log(result);
        },
        onError: function (result) {
          console.log("error");
          console.log(result);
        },
        onClose: function () {
          console.log(
            "customer closed the popup without finishing the payment"
          );
        },
      });
    }
  }, [token]);

  useEffect(() => {
    const midtransUrl = "https://app.sandbox.midtrans.com/snap/snap.js";

    let scriptTag = document.createElement("script");
    scriptTag.src = midtransUrl;

    const midtransClientKey = "SB-Mid-client-KYTVhja3CI8CJXxV";
    scriptTag.setAttribute("data-client-key", midtransClientKey);

    document.body.appendChild(scriptTag);

    return () => {
      document.body.removeChild(scriptTag);
    };
  }, []);

  return (
    <Container className="mt-5 p-5">
      <Card>
        <Card.Header>
          <h6>Invoices</h6>
        </Card.Header>
        <Card.Body>
          {!isFetching ? (
            <DataTable
              data={invoice}
              columns={[
                { selector: (row) => row.label },
                { cell: (row) => row.value },
              ]}
            />
          ) : null}
          <div className="d-grid gap-2">
            <Button variant="danger" onClick={process}>
              <h6>Bayar</h6>
            </Button>
          </div>
        </Card.Body>
      </Card>
    </Container>
  );
}
