import React from "react";
import { Form, Button, Card } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useDispatch } from "react-redux";
import { loginUser } from "../../app/api/auth";
import { userLogin } from "../../app/features/Auth/actions";
import { useHistory } from "react-router";
import { Link } from "react-router-dom/cjs/react-router-dom.min";

const schema = yup
  .object({
    email: yup
      .string()
      .email("Email harus valid")
      .required("Email harus diisi"),
    password: yup
      .string()
      .min(8, "Password minimal 8 karakter")
      .required("Password harus diisi"),
  })
  .required();

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

export default function Login() {
  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
  } = useForm({
    resolver: yupResolver(schema),
  });
  const [status, setStatus] = React.useState(statusList.idle);
  const dispatch = useDispatch();
  const history = useHistory();

  const onSubmit = async (formData) => {
    setStatus(statusList.process);
    const { data } = await loginUser(formData);
    if (data.error) {
      setError("password", {
        type: "invalidCredential",
        message: data.message,
      });
      setStatus(statusList.error);
    } else {
      const { user, token } = data;
      dispatch(userLogin({ user, token }));
      history.push("/");
    }
    setStatus(statusList.success);
  };

  return (
    <Card style={{ width: "35%", margin: "150px auto" }}>
      <Card.Header>
        <h4>Login</h4>
      </Card.Header>
      <Card.Body>
        <Form className="mb-3" onSubmit={handleSubmit(onSubmit)}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>
              <h6>Alamat Email</h6>
            </Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              isInvalid={errors.email}
              {...register("email")}
            />
            <Form.Control.Feedback type="invalid">
              {errors.email?.message}
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>
              <h6>Password</h6>
            </Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              isInvalid={errors.password}
              {...register("password")}
            />
            <Form.Control.Feedback type="invalid">
              {errors.password?.message}
            </Form.Control.Feedback>
          </Form.Group>
          <Button
            variant="danger"
            type="submit"
            disabled={status === statusList.process}
          >
            {status === statusList.process ? <h6>Memproses...</h6> : <h6>Login</h6>}
          </Button>
        </Form>
        <Link to="/register">
          <h6>Belum Punya Akun? Gass Bikin</h6>
        </Link>
      </Card.Body>
    </Card>
  );
}
