import React from "react";
import { Form, FormControl, InputGroup } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { setKeyword } from "../../app/features/Product/actions";

export default function SearchProduct() {
  const dispatch = useDispatch();
  return (
    <Form className="mb-3">
      <InputGroup>
        <FormControl
          type="text"
          placeholder="Cari barang..."
          onChange={(e) => dispatch(setKeyword(e.target.value))}
        />
      </InputGroup>
    </Form>
  );
}
