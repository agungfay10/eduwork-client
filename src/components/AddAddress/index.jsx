import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import React, { useState } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { createAddress } from "../../app/api/address";
import { useHistory } from "react-router";

const schema = yup
  .object({
    nama: yup.string().required("Nama alamat harus diisi"),
    detail: yup.string().required("Detail alamat harus diisi"),
    provinsi: yup.string().required("Provinsi harus diisi"),
    kabupaten: yup.string().required("Kabupaten harus diisi"),
    kecamatan: yup.string().required("Kecamatan harus diisi"),
    kelurahan: yup.string().required("kelurahan harus diisi"),
  })
  .required();

export default function AddAddress() {
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm({
    resolver: yupResolver(schema),
  });
  const [status, setStatus] = useState("idle");
  const history = useHistory();

  const onSubmit = async (formData) => {
    const payload = {
      nama: formData.nama,
      detail: formData.detail,
      provinsi: formData.provinsi,
      kabupaten: formData.kabupaten,
      kecamatan: formData.kecamatan,
      kelurahan: formData.kelurahan,
    };

    setStatus("process");
    const { data } = await createAddress(payload);
    if (!data.error) {
      setStatus("success");
      history.push("/account/address");
    }
  };

  // useEffect(() => {
  //   setValue("kabupaten", null);
  //   setValue("kecamatan", null);
  //   setValue("kelurahan", null);
  // }, [allField.provinsi, setValue]);
  // useEffect(() => {
  //   setValue("kecamatan", null);
  //   setValue("kelurahan", null);
  // }, [allField.kabupaten, setValue]);
  // useEffect(() => {
  //   setValue("kelurahan", null);
  // }, [allField.kecamatan, setValue]);

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <Row>
        <Col md={6}>
          <Form.Group className="mb-3" controlId="nama">
            <Form.Label>Nama</Form.Label>
            <Form.Control
              type="text"
              placeholder="Masukan nama alamat"
              isInvalid={errors.nama}
              {...register("nama")}
            />
            <Form.Control.Feedback type="invalid">
              {errors.nama?.message}
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group className="mb-3" controlId="detail">
            <Form.Label>Detail alamat</Form.Label>
            <Form.Control
              type="text"
              placeholder="Masukan detail alamat"
              as="textarea"
              isInvalid={errors.nama}
              rows={9}
              {...register("detail")}
            />
            <Form.Control.Feedback type="invalid">
              {errors.detail?.message}
            </Form.Control.Feedback>
          </Form.Group>
        </Col>
        <Col md={6}>
          <Form.Group className="mb-3" controlId="provinces">
            <Form.Label>Provinsi</Form.Label>
            <Form.Control
              type="text"
              placeholder="Masukan Provinsi"
              isInvalid={errors.provinsi}
              {...register("provinsi")}
            />
            <Form.Control.Feedback type="invalid">
              {errors.provinsi?.message}
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group className="mb-3" controlId="kabupaten">
            <Form.Label>Kabupaten</Form.Label>
            <Form.Control
              type="text"
              placeholder="Masukan Kabupaten"
              isInvalid={errors.kabupaten}
              {...register("kabupaten")}
            />
            <Form.Control.Feedback type="invalid">
              {errors.kabupaten?.message}
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group className="mb-3" controlId="kecamatan">
            <Form.Label>Kecamatan</Form.Label>
            {/* <CustomFormSelect
              onChange={(value) => updateValue("kecamatan", JSON.parse(value))}
              isInvalid={errors.kecamatan}
              code={getValues().kabupaten?.value}
              location="kecamatan"
              value={getValues()?.kecamatan?.value}
            /> */}
            <Form.Control
              type="text"
              placeholder="Masukan Kecamatan"
              isInvalid={errors.kecamatan}
              {...register("kecamatan")}
            />
            <Form.Control.Feedback type="invalid">
              {errors.kecamatan?.message}
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group className="mb-3" controlId="kelurahan">
            <Form.Label>Kelurahan</Form.Label>
            {/* <CustomFormSelect
              onChange={(value) => updateValue("kelurahan", JSON.parse(value))}
              isInvalid={errors.kelurahan}
              code={getValues().kecamatan?.value}
              location="kelurahan"
              value={getValues()?.kelurahan?.value}
            /> */}
            <Form.Control
              type="text"
              placeholder="Masukan Kelurahan"
              isInvalid={errors.kelurahan}
              {...register("kelurahan")}
            />
            <Form.Control.Feedback type="invalid">
              {errors.kelurahan?.message}
            </Form.Control.Feedback>
          </Form.Group>
        </Col>
      </Row>

      <div className="d-grid gap-2">
        <Button type="submit" variant="danger" disabled={status === "process"}>
          {status === "process" ? "Memproses..." : "Simpan"}
        </Button>
      </div>
    </Form>
  );
}
